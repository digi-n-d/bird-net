---
title: "Eldertop Kidnapping"
tags:
- "Eldertop Kidnapping Tracking Failure"
- "Eldertop Village"
---
With some effort the players can track the cultists to their camp.

The trail begins at the kidnap scene, goes through a river crossing, enters a fog bank, and ends at the cultist's camp. Each stage of the trail requires an increasingly difficult DC to succeed in tracking the cultists. Once a check has been passed the players know where that location is, and can return to it even if they leave and come back later. On a failure, roll on the encounter table. Subtract 5 from the roll per stage the players have already passed.

Regardless of success or failure, a check should progress time by 1d6+2 hours. If the players are quick enough they may be able to save the villagers.

On a critical success on any tracking roll or on the failure table, the players succeed in finding the next location and the planned encounter is replaced by the treants encounter.

- Find the river crossing from the scene of the disappearance - DC 15
- Find the fog bank from the river crossing - DC 18
- Find the camp in the fog - DC 20

When players succeed in a tracking roll they find the next location and start a planned encounter.

#### River crossing:
Describe tracks disappearing at the river.
A pair of oars abandoned on the beach with two shadows
If the players search upstream they get disadvantage on the next survival check. (no oars they can't paddle upstream)

#### Fog Bank:
Describe dense fog, out of the fog a shadow and a zombie appear. If the players don't go into denser fog they get disadvantage on the next survival check

#### Camp:
If the players arrive before nightfall of the first day they find the cultists with the villagers alive. If they arrive after nightfall they find the cultists and the villagers dead. If they arrive the next day they find only undead. On the second night the undead at the camp attack the village and leave a DC 10 tracking check straight there.

#### Treants:
The players encounter a gnome druid and two treants who are fleeing the fog. They are the last survivors of their grove, having fled when it fell to plague and then shadow attacks. They will not reveal where their home in the forest was located, but will share that the leader of their circle has stayed behind to fuel a magical barrier. They seek to reverse their grove's fate. but they will share what limited info they have. (Lizardfolk, sickness, followed by kidnappings and then shadow attacks).

They ask the players to spare healing items. If they do they are rewarded with a [pot-of-awakening](https://www.dndbeyond.com/magic-items/27113-pot-of-awakening)
