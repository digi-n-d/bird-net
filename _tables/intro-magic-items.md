---
title: "Intro Magic Items"
---
1. [a](https://www.dndbeyond.com/magic-items/27041-cloak-of-many-fashions)
2. [b](https://www.dndbeyond.com/magic-items/27042-clockwork-amulet)
3. [c](https://www.dndbeyond.com/magic-items/27049-hat-of-vermin)
4. [d](https://www.dndbeyond.com/magic-items/27076-mystery-key)
5. [e](https://www.dndbeyond.com/magic-items/27079-perfume-of-bewitching)
6. [f](https://www.dndbeyond.com/magic-items/27113-pot-of-awakening)
7. [g](https://www.dndbeyond.com/magic-items/27116-shield-of-expression)
8. [bag of beans](https://www.dndbeyond.com/magic-items/4579-bag-of-beans)
9. [i](https://www.dndbeyond.com/magic-items/27145-veterans-cane)
10. **Staffs**
  - [j](https://www.dndbeyond.com/magic-items/27139-staff-of-flowers)
  - [k](https://www.dndbeyond.com/magic-items/27137-staff-of-birdcalls)
  - [l](https://www.dndbeyond.com/magic-items/27136-staff-of-adornment)
20. [Cape of the Mountebank](https://www.dndbeyond.com/magic-items/4599-cape-of-the-mountebank)