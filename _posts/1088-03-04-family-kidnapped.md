---
title:  "Family Kidnapped"
tags:
- "Eldertop Village"
- "cultists"
- "Main Timeline"
---
A family was kidnapped from their [home]({% link _locations/kidnapping-scene.md %}) in [Eldertop Village]({% link _locations/eldertop-village.md %}) by [cultists]({% link _locations/abandoned-cultist-camp.md %}).