---
title: "Elderan Thistledown"
tags:
- "Eldertop Village"
---
**Elven Alchemist:**
   - Elderan is a wise and gentle elven Alchemist, with silver hair and a cloak adorned with healing runes. His apothecary is a hub of knowledge about the village's unique condition. Elderan explains that the mysterious shadows draining the life force of the villagers seem to be connected to an ancient druidic site in the nearby woods. He requests the players' assistance in investigating the source of the affliction.

   - **Knowledge of the returners:** He's met Zeypher once and since communicated by letter back and forth carried by the previous couriers with the supplies. He doesn't know them by any particular titles. He'll say he's getting a fair deal, but will admit that the price has gone up. He thinks the returners are motivated by gold alone and says so if asked. "Aye, they're motivated by `eyeing the gold dragon` *gold*... But who isnt in this world!? We pool what we have in this village. We can pay."