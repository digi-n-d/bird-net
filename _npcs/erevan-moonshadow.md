---
title: "Erevan Moonshadow"
tags:
- "Eldertop Village"
---
**Elven Blacksmith**
   - Erevan is a tall and graceful elf with jet-black hair and piercing blue eyes. Clad in forest green and silver, he moves with fluid movements as he tends to the fires of his forge. *Protected by the intricate elven runes with which they are adorned, Erevan shapes molten metal into works of art with his bare hands. Players can notice this fact if they try to observe him doing so. If they ask him about it he hill flatly deny it and change the subject.*
