---
title: "Anya Whisperleaf"
tags:
- "Eldertop Village"
---
**Halfling Innkeeper:**
   - Anya owns and operates the Eldertop Inn, a charming establishment with cozy rooms for visitors. Anya is a friendly halfling with a penchant for storytelling. She can share local folklore and legends, although she's usually far from accurate.
