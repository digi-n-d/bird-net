---
title: "Aelar Starwhisper"
tags:
- "Eldertop Village"
---
**Elven Priest of Tymora:**
   - Aelar is the charismatic elf priest of the Tymora Shrine in Eldertop Village. His emerald green robes and silver hair exude an aura of positivity and optimism. Known for engaging sermons and uplifting prayers, Aelar offers counsel and guidance to those in need, encouraging villagers to embrace life's uncertainties with hope and optimism.
