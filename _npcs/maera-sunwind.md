---
title: "Maera Sunwind"
tags:
- "Eldertop Village"
---
**Elven Healer**
   - The caretaker of Starleaf Sanctuary is , a compassionate elven healer whose gentle touch and soothing voice bring solace to the afflicted. With her flowing robes and intricate headdress adorned with starleaf blossoms, Maera exudes an aura of wisdom and serenity. Her apothecary is stocked with an array of remedies and potions, each carefully crafted to alleviate the symptoms of the village's mysterious malady.

   - **Knowledge of the returners:** She knows "Elderan has been paying for needed supplies. Expensive supplies. And a lot of them. But it's necessary..." But she doesn't know the returners by name. and she's never interacted with the couriers.
