---
title: "Audrey's Peculiarities Shoppe"
tags:
- "Eldertop Village"
---
Nestled among the quaint buildings of Leilon, Audrey's Peculiarities Shoppe stands out with its eccentric charm. The shop is a treasure trove of oddities, trinkets, and magical items, drawing both the curious and the whimsical.

Despite (or is it per?) its name, the proprietor is, peculiarly, not named Audrey.

The shop itself is a visual feast, adorned with whimsical decorations and dimly lit by enchanted lamps. Shelves are filled with a mesmerizing array of trinkets, from peculiar-looking statuettes to intricately designed potion bottles. Mystical aromas waft through the air, creating an otherworldly ambiance. Each corner of the shop seems to hold a secret, encouraging patrons to explore and discover the hidden wonders within.

[Isadora Blackthorn's]({% link _npcs/isadora-blackthorn.md %}) shop has become a destination for those seeking magical curiosities and unique items, turning Audrey's Peculiarities Shoppe into a haven for both seasoned adventurers and curious townsfolk alike. The tiefling enchantress, with her enigmatic charm, has become an integral part of Leilon's tapestry, leaving an indelible mark on the town's magical landscape.
