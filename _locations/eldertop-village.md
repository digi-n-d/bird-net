---
title: "Eldertop Village"
tags:
- "Eldertop Kidnapping"
- "Maera Sunwind"
- "Erevan Moonshadow"
- "Minor NPCs"
- "Elderan Thistledown"
- "Kidnapping Scene"
---
Nestled within the embrace of ancient trees, Eldertop Village exudes an otherworldly charm. The air is filled with the sweet scent of blooming flowers and the murmur of leaves rustling in the wind. The village is a collection of modest cottages with thatched roofs and wooden exteriors, surrounded by lush vegetation.

Eldertop Village has a tight-knit community that values nature and harmony. Villagers are generally wary of outsiders but will open up to those who genuinely seek to help.

A short time ago a series of unusual illnesses started to befall the members of the village causing the local healer to require increasingly large quantities of supplies. The Returners (a local mercenary group) were supplying that aid before the last two shipments went missing ('stolen'? by bandits), hence the player's quest to make the next delivery.

Furthering the woes currently befalling Eldertop, an entire family went missing from their home just the night before the players arrived.

**Locations:**
1. **Elderan's Apothecary:**
   - A quaint wooden building adorned with hanging herbs and flowers. This is where Elderan Thistledown, the village healer practices his craft. The apothecary is filled with shelves containing various potions, herbs, and mystical trinkets. A strong aroma of medicinal herbs permeates the air.

2. **Starleaf Sanctuary**
   - Starleaf Sanctuary stands as a solemn refuge amidst the weary village of Eldertop, its presence a stark reminder of the trials that have befallen its inhabitants. Nestled amidst the ancient trees, the infirmary's walls, crafted from woven branches, bear the weight of despair and uncertainty that hangs heavy in the air. Shimmering starleaf vines, once a symbol of hope, now cast a dim, flickering light within, struggling to pierce through the gloom that pervades the village.

3. **Silversong Forge**
   - Tucked away in a secluded corner of Eldertop Village, Silversong Forge is a modest yet elegant establishment owned and operated by an elven smith named Erevan Moonshadow. The shop is adorned with intricate wood carvings and hanging vines, blending seamlessly with the natural beauty of the surrounding forest. Inside, the air is rich with the scent of freshly worked metals and exotic woods. Visitors to Silversong Forge often find themselves entranced by the ethereal melodies that seem to linger in the air, earning the shop its enchanting name.

4. **The Eldertop Inn**
   - Nestled amidst the ancient trees, it serves as a beacon of warmth and respite in the midst of Eldertop Village's trials. Owned and operated by the affable halfling innkeeper, Anya Whisperleaf, the inn exudes an inviting charm despite the weariness that hangs over the village. With its cozy hearth, comfortable rooms, and hearty meals, the Eldertop Inn offers travelers a haven from the hardships of the outside world.

5. **Silvershade Square:**
   - The central square of Eldertop, Silvershade Square, is a communal gathering space with a large, ancient tree at its center. Villagers often congregate here for meetings, celebrations, or to share news. The tree is adorned with shimmering silver leaves, giving the square its name. In the evenings, the square is illuminated by enchanted lanterns, casting a warm glow over the surroundings. It's a serene place for players to interact with villagers and gather information.

6. **Tymora Shrine:**
   - **Description:** A compact but lively shrine dedicated to Tymora, the goddess of luck. Colorful banners and symbols of dice and coins decorate the space. The atmosphere is joyful and inviting, reflecting the unpredictable nature of luck.
   - **Services:** The Tymora Shrine offers luck-enhancing rituals, charms, and tokens for those seeking good fortune. Devotees come to pray for luck in endeavors, while the shrine also serves as a gathering place for gamblers and risk-takers.
